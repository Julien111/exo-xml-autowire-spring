/**
 * 
 *
 * @version 1.0<br/>
 */
package com.exo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Exemple.
 */
public final class Main {
	/** Main log. */
	private final static Log LOG = LogFactory.getLog(Main.class);

	/**
	 * Charge un fichier Spring.
	 *
	 * @param args
	 *            ne sert a rien
	 */
	public static void main(String[] args) {
		
		
		if (Main.LOG.isDebugEnabled()) {
			Main.LOG.debug("-- Debut -- ");
		}

		ClassPathXmlApplicationContext appContext = null;
		try {
			// Chargement du fichier
			appContext = new ClassPathXmlApplicationContext("mesBeans.xml");
			// // Récupération de notre instance de client
			 Client cl1 = (Client) appContext.getBean("client1");
//			 Client cl2 = (Client) appContext.getBean("client2");
//			 Client cl3 = (Client) appContext.getBean("client3");
//			 Client cl4 = (Client) appContext.getBean("client4");
			cl1.toString();
			// // Affichage
			 if (Main.LOG.isDebugEnabled()) {
				 Main.LOG.debug(cl1);
//				 Main.LOG.debug(cl2);
//				 Main.LOG.debug(cl3);
			 }

		} catch (BeansException e) {
			Main.LOG.fatal("Erreur", e);
			System.exit(-1);
		} finally {
			if (appContext != null) {
				appContext.close();
			}
		}
		if (Main.LOG.isDebugEnabled()) {
			Main.LOG.debug("-- Fin -- ");
		}
		System.exit(0);
	}
}