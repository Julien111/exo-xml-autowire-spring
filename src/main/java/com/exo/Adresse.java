package com.exo;

public class Adresse {
	//attr
	private String codePostal;
	private String adresse;
	private String ville;
	private String pays;
	
	//constr
	public Adresse() {
		super();
	}
	
	public Adresse(String codePostal, String adresse, String ville, String pays) {
		super();
		this.codePostal = codePostal;
		this.adresse = adresse;
		this.ville = ville;
		this.pays = pays;
	}

	@Override
	public String toString() {
		return "Adresse [codePostal=" + codePostal + ", adresse=" + adresse + ", ville=" + ville + ", pays=" + pays
				+ "]";
	}

	// Getters & Setters
	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}
	
	
	
	
}
