package com.exo;

import org.springframework.beans.factory.FactoryBean;

public class ClientFactory implements FactoryBean<Client>{

	private String prenom;
	private String nom;
	private int age = -1;
	
	@Override
	public Client getObject() throws Exception {	
			return new Client(prenom, nom, age,
					new Adresse("Inconnu","Inconnu","Inconnu","Inconnu"));		
	}

	@Override
	public Class<?> getObjectType() {
		
		return Client.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
//	public Client getInstance() {
//		return new Client("Inconnu","Inconnu",-1,
//				new Adresse("Inconnue","Inconnue","Inconnue","Inconnu"));
//		
//	}
}
